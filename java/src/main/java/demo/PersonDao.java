package demo;

/**
 * Created by vhphat on 12/25/2014.
 */
public interface PersonDao {
    public Person fetchPerson(Integer personID);
    public void update(Person person);
}
