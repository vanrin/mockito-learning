package tutorials;

import org.springframework.stereotype.Component;

/**
 * Created by vhphat on 12/19/2014.
 */
//@Service
@Component
public class PriceService {
    public int getActualPrice(Item item){
        throw new UnsupportedOperationException("Fail is not mocked!");
    }

    public int calculatePriceForOrder(Order order){
        int orderPrice = 0;
        for (Item item : order.getItems()){
            orderPrice += getActualPrice(item);
        }
        return orderPrice;
    }
}